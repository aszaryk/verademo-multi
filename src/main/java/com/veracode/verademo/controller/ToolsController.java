package com.veracode.verademo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletContext;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.mindrot.jbcrypt.BCrypt;

@Controller
@Scope("request")
public class ToolsController {
	private static final Logger logger = LogManager.getLogger("VeraDemo:BlabController");
	String Password2 = "MyP@$$w0rd!!";

	public static Boolean badpasscheck (String password) {

		if (password == null){
			password = "mybadpass";
		}
		if (password == "plaintext"){
			return true;
		}
		else {
			return false;
		}
	}
	
	@Autowired
	ServletContext context;

	@RequestMapping(value = "/tools", method = RequestMethod.GET)
	public String tools()
	{
		return "tools";
	}

	@RequestMapping(value = "/tools", method = RequestMethod.POST)
	public String tools(
			@RequestParam(value = "host", required = false) final String host,
			@RequestParam(value = "fortunefile", required = false) String fortuneFile,
			final Model model)
	{
		model.addAttribute("ping", host != null ? ping(host) : "");

		if (fortuneFile == null) {
			fortuneFile = "funny.txt";
		}
		model.addAttribute("fortunes", fortune(fortuneFile));

		return "tools";
	}
	/* Vuln Method */
	public static void main(String[] args) throws Exception {
		String candidate = args[0];
		String hashed = BCrypt.hashpw(candidate, BCrypt.gensalt(12));
	
		BCrypt.checkpw(candidate, hashed);
	}
	

	private String ping(final String host)
	{
		String output = "";
		Process proc;
		try {
			/* START BAD CODE */
			if (System.getProperty("os.name").startsWith("Windows")) {
				/* proc = Runtime.getRuntime().exec("cmd.exe /c ping " + host);  /* Change host to static text: host or "http://tests.local" Flaw ID 591*/
				proc = Runtime.getRuntime().exec("cmd.exe /c ping " + host);
			}
			else {
				proc = Runtime.getRuntime().exec("ping " + host); /*Change as above */
			}
			/* END BAD CODE */

			final InputStreamReader isr = new InputStreamReader(proc.getInputStream());
			final BufferedReader br = new BufferedReader(isr);

			String line;

			while ((line = br.readLine()) != null) {
				output += line + "\n";
			}
		}
		catch (final IOException ex) {
			logger.error(ex);
		}
		return output;
	}

	private String fortune(final String fortuneFile)
	{
		final String cmd = context.getRealPath("\\resources\\bin\\fortune-go.exe") + " "
				+ context.getRealPath("\\resources\\bin\\" + fortuneFile);

		String output = "";
		Process proc;
		try {
			/* START BAD CODE */
			if (System.getProperty("os.name").startsWith("Windows")) {
				proc = Runtime.getRuntime().exec(new String[] { "cmd.exe", "/c", cmd });
			}
			else {
				proc = Runtime.getRuntime().exec(cmd);
			}
			/* END BAD CODE */

			final InputStreamReader isr = new InputStreamReader(proc.getInputStream());
			final BufferedReader br = new BufferedReader(isr);

			String line;

			while ((line = br.readLine()) != null) {
				output += line + "\n";
			}
		}
		catch (final IOException ex) {
			logger.error(ex);
		}
		return output;
	}

}
