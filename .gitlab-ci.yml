variables:
    EXTRA_ARGS: "--update-advisor --recursive"
    MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
    MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"
    VERACODE_POLICYNAME: "Veracode Recommended Very High + SCA"

stages:
    - build_plus_sca
    - sec_pipeline
    - store
    - sec_tests
    - reporting

build_sca:
    image: maven:3.6.0-jdk-8
    stage: build_plus_sca
    only:
        refs:
            - master
    cache:
        paths:
            - .m2/repository
        key: "$CI_JOB_NAME"
    script:
        - mvn $MAVEN_CLI_OPTS clean package
        - curl -sSL https://download.sourceclear.com/ci.sh | bash -s -- scan $EXTRA_ARGS
    artifacts:
        paths:
            - target/
        expire_in: 5 hours
    allow_failure: true


veracode_pipeline:
  image: veracode/pipeline-scan:latest
  stage: sec_pipeline
  except:
    refs:
    - master
  script:
  - java -jar /opt/veracode/pipeline-scan.jar
    -vid $VERACODE_ID
    -vkey $VERACODE_KEY
    -f target/*.war
    --fail_on_severity="Very High, High"
  artifacts:
    name: ${CI_PROJECT_NAME}_${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}_devsecops-results
    paths:
    - results.json
    - results.txt
    expire_in: 5 hours
    when: always
  allow_failure: true
  
veracode_pipeline_policy:
    image: veracode/pipeline-scan:latest
    stage: sec_pipeline
    only:
      refs:
        - master
    script:  
        - java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_ID} -vkey ${VERACODE_KEY} --file target/*.war --policy_name "$VERACODE_POLICYNAME" 
    artifacts:  
        paths:  
            - results.json  
            - pipeline_scan_text_output.txt  
        when: always  
        name: "veracode-pipeline-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"  
    allow_failure: true 


veracode_static_policy:
    image: veracode/api-wrapper-java:latest
    stage: sec_tests
    only:
        refs:
            - master
    script:
        - java -jar /opt/veracode/api-wrapper.jar -vid $VERACODE_ID -vkey $VERACODE_KEY
          -action UploadAndScan -appname "$CI_PROJECT_NAME" -createprofile false 
          -filepath target/*.war -scantimeout 15
          -version "MASTER commit ${CI_COMMIT_SHA:0:8} pipeline $CI_PIPELINE_ID job $CI_JOB_ID"
    allow_failure: true

veracode_static_sandbox:
    image: veracode/api-wrapper-java:latest
    stage: sec_tests
    except:
        refs:
            - master
    script:
        - java -jar /opt/veracode/api-wrapper.jar -vid $VERACODE_ID -vkey $VERACODE_KEY
          -action UploadAndScan -appname "VeraDemo" -createprofile false -sandboxname "GitLab Pipelines" -createsandbox false
          -filepath target/*.war -scantimeout 30 -maxretrycount 5 -deleteincompletescan 2
          -version "BRANCH commit ${CI_COMMIT_SHA:0:8} pipeline $CI_PIPELINE_ID job $CI_JOB_ID"


veracode_dynamic_analysis:
    image: python:latest
    stage: sec_tests
    variables:
        VERACODE_API_KEY_ID: ${VERACODE_ID}
        VERACODE_API_KEY_SECRET: ${VERACODE_KEY}
    except:
        refs:
            - master
    before_script:
        - pip install -r vera-scripts/requirements.txt
    script:
        - python vera-scripts/create-da-scan-wrapper.py
        #- python vera-scripts/create-da-scan.py
        #- python vera-scripts/create-api-spec-scan.py
        #- python vera-scripts/create-da-scan-with-selenium.py
    allow_failure: true

Generate static analysis report and issues:
    image: node:latest
    stage: reporting
    except:
        refs:
            - master
    before_script:
        - npm ci
    script:
        - npm run results-import scan_type=policy profile_name=VeraDemo gitlab_token=${PRIVATE_TOKEN} gitlab_project=16703039 create_issue=true  
    artifacts:
        reports:
            sast: output-sast-vulnerabilites.json
        paths: 
            - output-sast-vulnerabilites.json
        when: always
        name: "veracode-POLICY-SCAN-RESULTS-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
